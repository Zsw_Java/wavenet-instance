package com.wavenet.maintenance.util;

import com.deepoove.poi.XWPFTemplate;
import com.deepoove.poi.data.*;
import com.deepoove.poi.util.BytePictureUtils;
import com.wavenet.maintenance.dao.entity.ReportDataDo;
import com.wavenet.maintenance.dao.entity.ReportParamDo;
import com.wavenet.maintenance.dao.entity.SubDataDo;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName ReportUtil
 * @Description TODO
 * @Author 叶方哲
 * @Date 2021/1/25 11:47
 * @Version 1.0
 */
@Service
public class ReportUtil {
    @Value("${templatePath}")
    private String templatePath;
    @Value("${subDataPath}")
    private String subDataPath;
    @Value("${filePath}")
    private String filePath;

    public String createDocx(ReportParamDo reportParamDo) throws IOException {
        ReportDataDo reportDataDo = covertData(reportParamDo);
        XWPFTemplate template = XWPFTemplate.compile(templatePath).render(reportDataDo);
        FileOutputStream out = new FileOutputStream(filePath);
        template.write(out);
        out.flush();
        out.close();
        template.close();
        return filePath;
    }

    private ReportDataDo covertData(ReportParamDo reportParamDo) throws IOException {
        ReportDataDo reportDataDo = new ReportDataDo();
        //纯文本
        reportDataDo.setText(reportParamDo.getText());
        //超链接文本
        reportDataDo.setLinkText(new HyperLinkTextRenderData(reportParamDo.getLinkText(),reportParamDo.getLinkAddress()));
        //本地图片
        reportDataDo.setPicturePath(new PictureRenderData(100,100,reportParamDo.getPicPath()));
        //网络图片
        reportDataDo.setPictureUrl(new PictureRenderData(100,100,reportParamDo.getPicUrl().substring(reportParamDo.getPicUrl().lastIndexOf(".")),BytePictureUtils.getUrlBufferedImage(reportParamDo.getPicUrl())));
        //列表
        List<TextRenderData> textRenderDatas = new ArrayList();
        reportParamDo.getNumberings().forEach(k->{
            textRenderDatas.add(new TextRenderData(k));
        });
        reportDataDo.setNumberic(new NumbericRenderData(textRenderDatas));
        SubDataDo subData = ReportDataDo.copy(reportDataDo);
        //嵌套模板
        reportDataDo.setSubData(new DocxRenderData(new File(subDataPath),new ArrayList(){{
            add(subData);
        }}));
        return reportDataDo;
    }
}
