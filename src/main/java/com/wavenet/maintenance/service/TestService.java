package com.wavenet.maintenance.service;

import com.wavenet.maintenance.dao.entity.Test;
import com.wavenetframework.boot.common.service.BaseService;
import com.wavenet.maintenance.web.query.TestQueryParam;
import com.wavenet.maintenance.web.vo.TestQueryVo;
import com.wavenetframework.boot.common.vo.Paging;

import java.io.Serializable;
import java.util.List;

/**
 * <pre>
 *  服务类
 * </pre>
 *
 * @author zll
 * @since 2021-01-25
 */
public interface TestService extends BaseService<Test> {

    /**
     * 保存
     *
     * @param test
     * @return
     * @throws Exception
     */
    boolean saveTest(Test test) throws Exception;

    /**
     * 修改
     *
     * @param test
     * @return
     * @throws Exception
     */
    boolean updateTest(Test test) throws Exception;

    /**
     * 删除
     *
     * @param id
     * @return
     * @throws Exception
     */
    boolean deleteTest(Long id) throws Exception;

    /**
     * 根据ID获取查询对象
     *
     * @param id
     * @return
     * @throws Exception
     */
    TestQueryVo getTestById(Serializable id) throws Exception;

    /**
     * 获取分页对象
     *
     * @param testQueryParam
     * @return
     * @throws Exception
     */
    Paging<TestQueryVo> getTestPageList(TestQueryParam testQueryParam) throws Exception;

    /**
     * execl导出
     * @return
     * @throws Exception
     */
    List<TestQueryVo> outExcel() throws Exception;

}
