package com.wavenet.maintenance.service.impl;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import com.wavenet.maintenance.dao.entity.Test;
import com.wavenet.maintenance.dao.mapper.TestMapper;
import com.wavenet.maintenance.service.TestService;
import com.wavenet.maintenance.web.query.TestQueryParam;
import com.wavenet.maintenance.web.vo.TestQueryVo;
import com.wavenetframework.boot.common.service.impl.BaseServiceImpl;
import com.wavenetframework.boot.common.vo.Paging;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import java.io.Serializable;
import java.util.List;


/**
 * <pre>
 *  服务实现类
 * </pre>
 *
 * @author zll
 * @since 2021-01-25
 */
@Slf4j
@Service
public class TestServiceImpl extends BaseServiceImpl<TestMapper, Test> implements TestService {

    @Autowired
    private TestMapper testMapper;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean saveTest(Test test) throws Exception {
        return super.save(test);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean updateTest(Test test) throws Exception {
        return testMapper.updateTest(test);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean deleteTest(Long id) throws Exception {
        return super.removeById(id);
    }

    @Override
    public TestQueryVo getTestById(Serializable id) throws Exception {
        return testMapper.getTestById(id);
    }

    @Override
    public Paging<TestQueryVo> getTestPageList(TestQueryParam testQueryParam) throws Exception {
        Page page = setPageParam(testQueryParam, OrderItem.desc("create_time"));
        IPage<TestQueryVo> iPage = testMapper.getTestPageList(page, testQueryParam);
        return new Paging(iPage);
    }

    @Override
    public List<TestQueryVo> outExcel() throws Exception {
        return testMapper.outExcel();
    }

}
