package com.wavenet.maintenance.fastdfs;

import lombok.extern.slf4j.Slf4j;
import org.csource.common.MyException;
import org.csource.common.NameValuePair;
import org.csource.fastdfs.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

@Slf4j
public class FastDFSClient {
    static {
        try {
//            ClientGlobal.init("classpath:fdfs_client.conf");
            ClientGlobal.init("fdfs_client.conf");
            //ClientGlobal.setG_tracker_http_port(8888);
            System.out.println("FastDFS Client 启动成功！");
            log.info("FastDFS Client 启动成功！!!!");
        } catch (Exception e) {
            System.out.println("FastDFS Client 启动失败！");
            log.error("FastDFS Client 启动失败！");

        }
    }

    public static String[] upload(FastDFSFile file) {
        log.info("File Name: " + file.getName() + "File Length:" + file.getContent().length);

        NameValuePair[] meta_list = new NameValuePair[1];
        meta_list[0] = new NameValuePair("author", file.getAuthor());
        long startTime = System.currentTimeMillis();
        String[] uploadResults = null;
        StorageClient storageClient=null;
        try {
            storageClient = getTrackerClient();
            uploadResults = storageClient.upload_file(file.getContent(), file.getExt(), meta_list);
        } catch (IOException e) {
            log.error("IO Exception when uploadind the file:" + file.getName(), e);
        } catch (Exception e) {
            log.error("Non IO Exception when uploadind the file:" + file.getName(), e);
        }
        log.info("upload_file time used:" + (System.currentTimeMillis() - startTime) + " ms");

        if (uploadResults == null && storageClient!=null) {
            log.error("upload file fail, error code:" + storageClient.getErrorCode());
        }
        String groupName = uploadResults[0];
        String remoteFileName = uploadResults[1];

        log.info("upload file successfully!!!" + "group_name:" + groupName + ", remoteFileName:" + " " + remoteFileName);
        return uploadResults;
    }

    public static FileInfo getFile(String groupName, String remoteFileName) {
        try {
            StorageClient storageClient = getTrackerClient();
            return storageClient.get_file_info(groupName, remoteFileName);
        } catch (IOException e) {
            log.error("IO Exception: Get File from Fast DFS failed", e);
        } catch (Exception e) {
            log.error("Non IO Exception: Get File from Fast DFS failed", e);
        }
        return null;
    }

    public static InputStream downFile(String groupName, String remoteFileName) {
        try {
            StorageClient storageClient = getTrackerClient();
            byte[] fileByte = storageClient.download_file(groupName, remoteFileName);
            InputStream ins = new ByteArrayInputStream(fileByte);
            return ins;
        } catch (IOException e) {
            log.error("IO Exception: Get File from Fast DFS failed", e);
        } catch (Exception e) {
            log.error("Non IO Exception: Get File from Fast DFS failed", e);
        }
        return null;
    }


    public static void deleteFile(String groupName, String remoteFileName)
            throws Exception {
        StorageClient storageClient = getTrackerClient();
        int i = storageClient.delete_file(groupName, remoteFileName);
        log.info("delete file successfully!!!" + i);
    }

    /**
     * 使用multipartFile 进行文件上传
     * @param multipartFile
     * @return
     * @throws IOException
     */
    public static String saveFile(MultipartFile multipartFile) throws IOException{
        String fileName = multipartFile.getOriginalFilename();//获取文件名
        String ext = fileName.substring(fileName.lastIndexOf(".")+1);
        byte[] file_buff = null;
        InputStream inputStream = multipartFile.getInputStream();
        if(inputStream != null){
            int len = inputStream.available();
            file_buff = new byte[len];
            inputStream.read(file_buff);
        }
        inputStream.close();
        FastDFSFile file = new FastDFSFile(fileName, file_buff, ext);
        String[] fileAbsolutePath = FastDFSClient.upload(file);//上传至faseDFS服务器
        String url = FastDFSClient.getTrackerUrl();
        String newUrl = url.substring(0,url.lastIndexOf(":")+1);
//        String path = "http://222.66.154.70:2062/"+fileAbsolutePath[0]+ "/"+fileAbsolutePath[1];
        String path = "http://116.239.7.233:8088/file/"+fileAbsolutePath[0]+ "/"+fileAbsolutePath[1];
//        String path = newUrl+"8888/"+fileAbsolutePath[0]+ "/"+fileAbsolutePath[1];
        return path;

    }

    public static String[] getUrl(String httpUrl){
        String[] str = new String[2];
        int secondindex = httpUrl.indexOf("group", 1);
        httpUrl = httpUrl.substring( secondindex);
        secondindex = httpUrl.indexOf("/", 1);
        str[0] = httpUrl.substring(0, secondindex);
        str[1]= httpUrl.substring(secondindex + 1);
        return str;
    }

    public static StorageServer[] getStoreStorages(String groupName)
            throws IOException, MyException {
        TrackerClient trackerClient = new TrackerClient();
        TrackerServer trackerServer = trackerClient.getTrackerServer();//getTrackerServer();//getConnection();
        return trackerClient.getStoreStorages(trackerServer, groupName);
    }

    public static ServerInfo[] getFetchStorages(String groupName,
                                                String remoteFileName) throws IOException, MyException {
        TrackerClient trackerClient = new TrackerClient();
        TrackerServer trackerServer = trackerClient.getTrackerServer();//getTrackerServer();//getConnection();
        return trackerClient.getFetchStorages(trackerServer, groupName, remoteFileName);
    }

    public static String getTrackerUrl() throws IOException {
        return "http://"+getTrackerServer().getInetSocketAddress().getHostString()+":"+ClientGlobal.getG_tracker_http_port()+"/";
    }

    private static StorageClient getTrackerClient() throws IOException {
        TrackerServer trackerServer = getTrackerServer();
        StorageClient storageClient = new StorageClient(trackerServer, null);
        return  storageClient;
    }

    private static TrackerServer getTrackerServer() throws IOException {
        TrackerClient trackerClient = new TrackerClient();
        TrackerServer trackerServer = trackerClient.getTrackerServer();//getTrackerServer();//getConnection();
        return  trackerServer;
    }


}
