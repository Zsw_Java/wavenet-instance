package com.wavenet.maintenance.web;

import com.wavenet.maintenance.dao.entity.Test;
import com.wavenet.maintenance.service.TestService;
import com.wavenet.maintenance.util.ExcelUtils;
import com.wavenet.maintenance.web.query.TestQueryParam;
import com.wavenet.maintenance.web.vo.TestQueryVo;
import com.wavenetframework.boot.common.api.ApiResult;
import com.wavenetframework.boot.common.controller.BaseController;
import com.wavenetframework.boot.common.vo.Paging;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

/**
 * <pre>
 *  前端控制器
 * </pre>
 *
 * @author zll
 * @since 2021-01-25
 */
@Slf4j
@RestController
@RequestMapping("/test")
@Api(tags = {"规范测试API"})
public class TestController extends BaseController {

    @Autowired
    private TestService testService;

    /**
     * 添加
     */
    @PostMapping("/add")
//    @RequiresPermissions("test:add")
    @ApiOperation(value = "添加Test对象", notes = "添加", response = ApiResult.class)
    public ApiResult<Boolean> addTest(@Valid @RequestBody Test test) throws Exception {
        boolean flag = testService.saveTest(test);
        return ApiResult.result(flag);
    }

    /**
     * 修改
     */
    @PostMapping("/update")
//    @RequiresPermissions("test:update")
    @ApiOperation(value = "修改Test对象", notes = "修改", response = ApiResult.class)
    public ApiResult<Boolean> updateTest(@Valid @RequestBody Test test) throws Exception {
        boolean flag = testService.updateTest(test);
        return ApiResult.result(flag);
    }

    /**
     * 删除
     */
    @PostMapping("/delete/{id}")
//    @RequiresPermissions("test:delete")
    @ApiOperation(value = "删除Test对象", notes = "删除", response = ApiResult.class)
    public ApiResult<Boolean> deleteTest(@PathVariable("id") Long id) throws Exception {
        boolean flag = testService.deleteTest(id);
        return ApiResult.result(flag);
    }

    /**
     * 获取
     */
    @GetMapping("/info/{id}")
//    @RequiresPermissions("test:info")
    @ApiOperation(value = "输入ID，查询测试对象的详情。", notes = "查看", response = TestQueryVo.class)
    //提供给APP和前端的接口最好共用，且做好接口中文注释
    public ApiResult<TestQueryVo> getTest(@PathVariable("id") Long id) throws Exception {
        TestQueryVo testQueryVo = testService.getTestById(id);
        return ApiResult.ok(testQueryVo);
    }

    /**
     * 分页列表
     */
    @PostMapping("/page")
//    @RequiresPermissions("test:page")
    @ApiOperation(value = "获取Test分页列表", notes = "分页列表", response = TestQueryVo.class)
    public ApiResult<Paging<TestQueryVo>> getTestPageList(@Valid @RequestBody TestQueryParam testQueryParam) throws Exception {
        Paging<TestQueryVo> paging = testService.getTestPageList(testQueryParam);
        //返回值必须是标准的json格式
        return ApiResult.ok(paging);
    }

    /**
     * Excel导出 郭春雷
     */
    @GetMapping("/Excel-Test")
    @ApiOperation(value = "Excel导出", notes = "Excel导出")
    public void outExcel(HttpServletResponse response) throws Exception {
        List<TestQueryVo> list = testService.outExcel();
        ExcelUtils.exportExcel(list,"测试信息","测试信息",TestQueryVo.class,"测试信息",response);
    }

    /**
     * list 郭春雷
     */
    @GetMapping("/List")
    @ApiOperation(value = "获取list", notes = "获取list")
    public ApiResult list() throws Exception {
        List<TestQueryVo> list = testService.outExcel();
        return ApiResult.ok(list);
    }



}

