package com.wavenet.maintenance.web.vo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.sql.Date;

/**
 * <pre>
 *  查询结果对象
 * </pre>
 *
 * @author zll
 * @date 2021-01-25
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "TestQueryVo对象", description = "查询参数")
public class TestQueryVo implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @Excel(name = "ID", orderNum = "1")
    private String id;

    @ApiModelProperty(value = "名称")
    @TableField("NAME")
    @Excel(name = "名字", orderNum = "2")
    private String name;

    @ApiModelProperty(value = "时间")
    @TableField("TIME")
    @Excel(name = "时间", orderNum = "3")
    //时间类型，数据库是什么格式，后端返回什么格式
    private Date time;

}