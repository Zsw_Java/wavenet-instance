package com.wavenet.maintenance.web;

import com.deepoove.poi.XWPFTemplate;
import com.wavenet.maintenance.dao.entity.ReportDataDo;
import com.wavenet.maintenance.dao.entity.ReportParamDo;
import com.wavenet.maintenance.dao.entity.Test;
import com.wavenet.maintenance.service.TestService;
import com.wavenet.maintenance.util.ReportUtil;
import com.wavenet.maintenance.web.query.TestQueryParam;
import com.wavenet.maintenance.web.vo.TestQueryVo;
import com.wavenetframework.boot.common.api.ApiResult;
import com.wavenetframework.boot.common.controller.BaseController;
import com.wavenetframework.boot.common.vo.Paging;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import oracle.jdbc.proxy.annotation.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

/**
 * <pre>
 *  前端控制器
 * </pre>
 *
 * @author zll
 * @since 2021-01-25
 */
@Slf4j
@RestController
@RequestMapping("/createReport")
@Api(tags = {"生成模板报告"})
public class CreateReportController extends BaseController {

    @Autowired
    private ReportUtil reportUtil;

    @PostMapping("/createReport")
    public ApiResult createReport(@RequestBody ReportParamDo reportParamDo, HttpServletResponse response) throws IOException {
        String template = reportUtil.createDocx(reportParamDo);
        return ApiResult.ok(template);
    }

}

