package com.wavenet.maintenance.dao.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wavenet.maintenance.dao.entity.Test;
import com.wavenet.maintenance.web.query.TestQueryParam;
import com.wavenet.maintenance.web.vo.TestQueryVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;

/**
 * <pre>
 *  Mapper 接口
 * </pre>
 *
 * @author zll
 * @since 2021-01-25
 */
@Repository
public interface TestMapper extends BaseMapper<Test> {

    /**
     * 根据ID获取查询对象
     *
     * @param id
     * @return
     */
    TestQueryVo getTestById(Serializable id);

    /**
     * 获取分页对象
     *
     * @param page
     * @param testQueryParam
     * @return
     */
    IPage<TestQueryVo> getTestPageList(@Param("page") Page page, @Param("param") TestQueryParam testQueryParam);


    //根据ID修改test表
    Boolean updateTest(@Param("param")Test test);

    /**
     * execl导出
     * @return
     */
    List<TestQueryVo> outExcel();
}
