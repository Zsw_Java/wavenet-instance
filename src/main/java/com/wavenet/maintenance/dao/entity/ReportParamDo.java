package com.wavenet.maintenance.dao.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.List;

/**
 * @ClassName ReportParamDo
 * @Description TODO
 * @Author 叶方哲
 * @Date 2021/1/25 13:13
 * @Version 1.0
 */
@Data
public class ReportParamDo {
    @ApiModelProperty(name = "text",value = "普通文本内容")
    private String text;
    @ApiModelProperty(name = "linkText",value = "超链接文本内容")
    private String linkText;
    @ApiModelProperty(name = "linkAddress",value = "超链接文本链接地址")
    private String linkAddress;
    @ApiModelProperty(name = "picUrl",value = "网络图片地址")
    private String picUrl;
    @ApiModelProperty(name = "picPath",value = "本地图片地址")
    private String picPath;
    @ApiModelProperty(name = "numberings",value = "列表(文字)")
    private List<String> numberings;
}
