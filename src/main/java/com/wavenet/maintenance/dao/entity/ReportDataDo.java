package com.wavenet.maintenance.dao.entity;

import com.deepoove.poi.data.*;
import lombok.Data;

/**
 * @ClassName ReportDataDo
 * @Description TODO
 * @Author 叶方哲
 * @Date 2021/1/25 11:52
 * @Version 1.0
 */
@Data
public class ReportDataDo {
    //普通文本 {{text}}
    private String text;
    //超链接文本 {{linkText}}
    private HyperLinkTextRenderData linkText;
    //本地图片 {{@picture}}
    private PictureRenderData picturePath;
    //网络图片 {{@picture}}
    private PictureRenderData pictureUrl;
    //列表 {{*numberic}}
    private NumbericRenderData numberic;
    //嵌套 {{+subData}}
    private DocxRenderData subData;
    public static SubDataDo copy(ReportDataDo reportDataDo){
        SubDataDo subDataDo = new SubDataDo();
        subDataDo.setText(reportDataDo.getText());
        subDataDo.setLinkText(reportDataDo.getLinkText());
        subDataDo.setPicturePath(reportDataDo.getPicturePath());
        subDataDo.setPictureUrl(reportDataDo.getPictureUrl());
        subDataDo.setNumberic(reportDataDo.getNumberic());
        return subDataDo;
    }
}
