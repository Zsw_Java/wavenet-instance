package com.wavenet.maintenance.dao.entity;

import com.deepoove.poi.data.DocxRenderData;
import com.deepoove.poi.data.HyperLinkTextRenderData;
import com.deepoove.poi.data.NumbericRenderData;
import com.deepoove.poi.data.PictureRenderData;
import lombok.Data;

/**
 * @ClassName ReportDataDo
 * @Description TODO
 * @Author 叶方哲
 * @Date 2021/1/25 11:52
 * @Version 1.0
 */
@Data
public class SubDataDo {
    //普通文本 {{text}}
    private String text;
    //超链接文本 {{linkText}}
    private HyperLinkTextRenderData linkText;
    //本地图片 {{@picture}}
    private PictureRenderData picturePath;
    //网络图片 {{@picture}}
    private PictureRenderData pictureUrl;
    //列表 {{*numberic}}
    private NumbericRenderData numberic;
}
